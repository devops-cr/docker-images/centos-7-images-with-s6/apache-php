FROM centos:7-christos

RUN yum install -y httpd \
    mod_ssl \
 && yum clean all

ARG HTTP_PORT=80 \
    HTTPS_PORT=443

RUN yum install -y \
    epel-release \
    http://rpms.remirepo.net/enterprise/remi-release-7.rpm \
 && sed -i '0,/enabled/{s/enabled=0/enabled=1/}' /etc/yum.repos.d/remi-php74.repo \
 && yum install -y \
    php php-common php-opcache php-mcrypt php-cli php-gd php-curl php-mysqlnd \
 && yum clean all


COPY var/ /var/
COPY etc/ /etc/

ENV \
    SSL_ENABLED=false \
    SECURITY_ENABLED=false \
    HTTP_PORT=$HTTP_PORT \
    HTTPS_PORT=$HTTPS_PORT


EXPOSE $HTTP_PORT $HTTPS_PORT
